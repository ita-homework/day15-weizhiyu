# O

- Roles in project: learn what is QA / BA / UX / DEV / Tech Lead and their roles in project. Also, we identified the roles of each team member in the simulation project.
- Agile Activities: Agile activities include IPM, Story Kick-off, Desk check, etc. In class, we drew a user journey map, learned the user's pain points, and designed our own products. Then we made a user story, describing the user usage process and sorting out the features of our product.
- retrospective: This is the second time we have made a retrospective, in which we looked at what we did WELL/NOT WELL. For example, in terms of teamwork, we did a better job and prepared our presentation better than before. But there are also aspects that are not well, such as code smell in the project code

# R

I'm a little nervous

# I



Due to the complexity of the task, many details have not been clarified.

# D

We will use the weekend to clarify the requirements and specify the plan via online and offline meeting